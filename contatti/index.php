<?php
/*Codice della mappa dei LUG italiani
  Copyright (C) 2010-2022  Italian Linux Society - http://www.linux.it

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
?>
<?php
header("Location: https://www.ils.org/contatti/");
die();
require_once ('../funzioni.php');
lugheader ('Contatti');
?>

<div class="container mt-3">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<p>
				I Manovali della LugMap sono piuttosto brutti ma non mordono, possono essere raggiunti sulla relativa <a href="http://lists.linux.it/listinfo/lugmap" class="generalink">mailing list</a> (si badi che la lista è pubblica, cos&igrave; come l'archivio), oppure ai loro contatti diretti.
			</p>

			<div class="row">
				<div class="col-md-6">
					<img src="andrea_gelmini.png" class="img-fluid">
					<p>
						Andrea Gelmini<br>
						(residente a Brescia)<br>telefonicamente al <a href="tel:328-72-96-628" class="generalink">328/72-96-628</a><br>via <a href="mailto:andrea.gelmini@lugbs.linux.it" class="generalink">mail</a><br>o attraverso <a href="http://www.facebook.com/andrea.gelmini" class="generalink">Facebook</a>
					</p>
				</div>
				<div class="col-md-6 text-right">
					<img src="roberto_guido.png" class="img-fluid">
					<p>
						Roberto Guido<br>
						via <a class="generalink" href="mailto:bob@linux.it">mail</a><br>o attraverso <a class="generalink" href="http://twitter.com/madbob">Twitter</a>
					</p>
				</div>
			</div>

			<hr>

			<p>
				Puoi partecipare direttamente, sia alla stesura del codice che del database, sfruttando il <a class="generalink" href="https://gitlab.com/ItalianLinuxSociety/LugMap/-/commits/master">repository GitLab</a>. Il codice sorgente del sito &egrave; distribuito con licenza <a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html">AGPLv3</a>, mentre i dati sono pubblicati in licenza <a href="http://creativecommons.org/publicdomain/zero/1.0/legalcode">CC0</a>.
			</p>
			<p>
				Puoi sapere tutti, ma proprio tutti, i dettagli su chi, cosa, come mantiene la LugMap, dai file pubblici agli script di automazione interna, leggendo la <a class="generalink" href="https://gitlab.com/ItalianLinuxSociety/LugMap/-/raw/docs/Guida_Intergalattica_Alla_LugMap.pdf?inline=false">Guida Intergalattica alla LugMap</a>.
			</p>

			<hr>

			<div id="title"><h2 class="titolino">Ringraziamenti</h2></div>
			<p>Il gruppo ringrazia sentitamente (davvero!) tutti coloro i quali hanno dedicato energie e tempo
			per il miglioramento di questo progetto. In particolare un sentito grazie a:
			<ul class="spaced-list">
				<li><a href="mailto:alessandro.cinelli@lugbs.linux.it" class="generalink">Alessandro Cinelli</a></li>
				<li><a href="mailto:andrea.occhi@lugbs.linux.it" class="generalink">Andrea Occhi</a></li>
				<li><a href="mailto:luca@linux.it" class="generalink">Luca Menini</a></li>
				<li><a href="mailto:luisa.ravelli@lugbs.linux.it" class="generalink">Luisa Ravelli</a></li>
				<li><a href="mailto:michele@tameni.it" class="generalink">Michele Tameni</a></li>
				<li><a href="mailto:nicolo.cristini@lugbs.linux.it" class="generalink">Nicol&ograve; Cristini</a></li>
				<li><a href="http://www.kalamun.org/" class="generalink">Roberto Kalamun Pasini</a></li>
			</ul>
			</p>
		</div>
	</div>
</div>
<?php
  lugfooter ();
?>
