<?php
/*Codice della mappa dei LUG italiani
  Copyright (C) 2010-2024 Italian Linux Society, contributori LUGMap

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
?>
<?php
# parsing della richiesta della regione, che può arrivare da 4 tipi di url:
# http://www.linux.it/LUG (indirizzo storico)
# lug-list.phtml?reg=nome - formato storico   (vogliamo che dia un redirect al definitivo)
# regione.php?reg=nome  - formato di transizione (idem)
# /regione/nome-regione - formato di transizione (idem)
# /nome-regione/        - definitivo

if (preg_match('/index\.php$/', $_SERVER["SCRIPT_NAME"])) { # se sono nel file index.php, allora sono stato invocato da /nome-regione/
  require_once ('../funzioni.php');
  $regione = substr(dirname($_SERVER["SCRIPT_NAME"]), 1); # estraggo la regione dal percorso
  if (array_key_exists ($regione, $elenco_regioni)) { # lasciamo il controllo, ma in ogni caso dovremmo ottenere un 404
    $db_file = _configuration_path_relative($regione);
    $db_regione = all_lugs_lines_in_region($regione);
    $title = 'LugMap: '. $elenco_regioni[$regione];
  } else {
            header("Location: https://lugmap.linux.it/"); }

} else { # qui se sono stato invocato alla vecchia maniera
  require_once ('funzioni.php');
  if (isset ($_REQUEST["reg"])) {
    if (array_key_exists ($_REQUEST["reg"], $elenco_regioni)) { # lasciamo il controllo, ma probabilmente non serve più
      switch ($_REQUEST["reg"]) {
          case "emilia":
            $regione_da_reindirizzare = "emilia-romagna";
            break;
          case "friuli":
            $regione_da_reindirizzare = "friuli-venezia-giulia";
            break;
          case "trentino":
            $regione_da_reindirizzare = "trentino-alto-adige";
            break;
          case "valle":
            $regione_da_reindirizzare = "valle-daosta";
            break;
          default:
            $regione_da_reindirizzare = $_REQUEST["reg"];
      }
      header("HTTP/1.1 301 Moved Permanently");
      header("Location: https://lugmap.linux.it/".$regione_da_reindirizzare."/");
      exit();
    } else {
      header("location: https://lugmap.linux.it/");
    }
  } else {
    $db_regione = all_lugs_lines_flat();

    $db_file = null;
    $regione = 'Italia';
    $title = 'LugMap: I LUG italiani';
  }
}

lugheader ($title);

?>

<div id="center" class="mt-3">
  <h1><?php printf(
    "%s - Linux User Group disponibili",
    htmlentities( substr($title, 8) )
  ) ?></h1>

  <?php if (!$db_regione): ?>
    <p>Questo sembra un territorio inesplorato.</p>
  <?php endif ?>

  <p class="fromRegionLinks">
    <a href="/">&raquo; torna alla LugMap&nbsp;</a>
  </p>
  <?php if($db_regione): ?>
  <table id="lugListTable">
    <thead>
        <tr>
          <th>Provincia</th>
          <th>Zona</th>
          <th>Denominazione</th>
        </tr>
     </thead>
     <tfoot>
      <tr>
        <td colspan="3"></td>
        </tr>
    </tfoot>
    <tbody>
      <?php $nriga = 1; ?>
      <?php foreach($db_regione as $linea):
        $campi         = lug_data($linea); # estrazione dei campi
        $provincia     = $campi[0];
        $denominazione = $campi[1];
        $zona          = $campi[2];
        $sito          = $campi[3];
        # stampa dei campi ?>
        <tr class="row_<?php echo ($nriga % 2); ?>">
         <td class="province"><?php echo $provincia ?></td>
         <td><a href="/mappa/?zoom=<?php echo str_replace (' ', '_', $denominazione) ?>"><?php echo $zona ?></a></td>

         <?php if (strncmp($denominazione, 'ILS ', 4) == 0): ?>
          <td><a class="generalink" style="display: inline-grid" href="<?php echo $sito ?>" target="_blank"><?php echo $denominazione ?> <img src="https://www.ils.org/images/italian_linux_society_round_small.png" alt="Sezione Locale ILS" style="height: 30px; margin: auto; padding-bottom: 6px"></a></td>
         <?php else: ?>
          <td><a class="generalink" href="<?php echo $sito ?>" target="_blank"><?php echo $denominazione ?></a></td>
         <?php endif ?>
        </tr>
        <?php $nriga++ ?>
      <?php endforeach ?>
    </tbody>
   </table>

   <p class="fromRegionLinks mt-2">

    <?php if ($db_file): ?>
        <a href="<?php echo $db_file ?>">&raquo; Elenco in formato CSV&nbsp;</a><br />
    <?php else: ?>
        <br />
    <?php endif ?>

   <?php ultimo_aggiornamento() ?>

   </p>

   <?php endif ?>

   <p>Conosci un user group in quest'area che manca in questo elenco? Per aggiornamenti, <a href="https://www.ils.org/contatti/">contatta Italian Linux Society</a>!</p>
</div>

<?php
  lugfooter ();
?>
