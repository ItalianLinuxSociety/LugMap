# LugMap - Mappa dei Linux User Group in Italia

La [LugMap](https://lugmap.linux.it/) è un sito web che mette in contatto le
realtà interessate alla diffusione del Software Libero in Italia.

La LugMap deve il suo nome ai *user group* legati a Linux,
in quanto punta di diamante del Software Libero (ma non si limita ai "gruppi Linux").

Visita e contribuisci alla mappa degli user group attivi in Italia:

https://lugmap.linux.it/

## Struttura del repository

Il ramo principale per motivi storici si chiama `lugmap.linux.it`. Il nome di questo ramo potrebbe cambiare in futuro.

Il ramo principale ha queste cartelle:

- `db/` - database con i dati dei LUG
- `guida/` - documentazione per nuovi LUG, e anche documentazione tecnica
- `script/` - cose simpatiche

Altri rami di sviluppo sono deprecati (https://gitlab.com/ItalianLinuxSociety/LugMap/-/issues/50).

## Come collaborare

Ecco alcuni esempi per collaborare in questo repository:

- puoi aiutare nel bug tracker:
  https://gitlab.com/ItalianLinuxSociety/LugMap/-/issues
- puoi creare una tua "Fork" di questo repository e fare modifiche in completa autonomia:
   https://gitlab.com/ItalianLinuxSociety/LugMap
- puoi discutere e proporre le tue modifiche, per farle pubblicare nel progetto principale.
- oppure puoi anche soltanto fare una donazione: https://www.ils.org/info/#donazioni

È anche disponibile una documentazione piuttosto dettagliata:

https://gitlab.com/ItalianLinuxSociety/LugMap/-/raw/docs/Guida_Intergalattica_Alla_LugMap.pdf?inline=false

Grazie per ogni contributo! Grazie per ogni critica costruttiva, e per ogni proposta di codice.

## Deploy

Il file `data/geo.txt` viene generato anche automaticamente entrando nella directory `mappa/` e lanciando:

    ./generator.php

Attenzione: impiega diverso tempo per contattare OpenStreetMap. Tenta anche di inviare delle email.

Il sito in produzione è sul server `itgate-kirk` ed è aggiornato automaticamente ogni tot. ore dal branch `lugmap.linux.it`.

https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/INVENTORY.md

## Contatti

Ecco alcuni punti di contatto per discutere velocemente con la community:

- gruppo Telegram `@ItalianLinuxSociety` (sezione /Developer)
- mailing list pubblica lugmap@linux.it - http://lists.linux.it/listinfo/lugmap
- puoi anche fare scherzi telefonici al maintainer Andrea Gelmini - andrea.gelmini@lugbs.linux.it - 328/7296628

## Licenze

Il codice che genera il sito web è rilasciato con licenza GNU AGPL.

https://www.gnu.org/licenses/agpl-3.0.en.html

Gli script e tutto l'apparato di backend è rilasciato con licenza GNU GPL.

https://www.gnu.org/licenses/gpl-3.0.html

I dati grezzi esposti nella cartella "db/" (fra cui nomi e coordinate) vengono rilasciati con lo strumento CC0.

https://creativecommons.org/publicdomain/zero/1.0/

Tutto questo tranne dove diversamente indicato, ovviamente.
