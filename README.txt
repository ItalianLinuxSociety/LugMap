== Licenza ==

Tutto il codice utilizzato per il servizio WEB
viene rilasciato con licenza AGPL, mentre gli
script e tutto l'apparato di backend gode della
licenza GPL; tranne dove diversamente indicato,
ovviamente.
